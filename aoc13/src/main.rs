extern crate common;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum Dir {
    Left,
    Straight,
    Right,
}

impl Dir {
    fn next(&self) -> Self {
        match self {
            Self::Left => Self::Straight,
            Self::Straight => Self::Right,
            Self::Right => Self::Left,
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
struct Cart {
    i: usize,
    j: usize,
    on: char,
    cc: Dir, // current corner direction
}

impl Cart {
    fn from(ii: usize, jj: usize, d: &char) -> Self {
        Self {
            i: ii,
            j: jj,
            on: if ['<', '>'].contains(d) { '-' } else { '|' },
            cc: Dir::Left,
        }
    }

    fn corner(&mut self) -> Dir {
        let dir = self.cc;
        self.cc = self.cc.next();
        dir
    }
}

impl PartialOrd for Cart {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Cart {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        let cmp_i = self.i.cmp(&other.i);
        let cmp_j = self.j.cmp(&other.j);
        cmp_i.then(cmp_j)
    }
}

struct Track {
    width: usize,
    height: usize,
    data: Vec<char>,
}

impl Track {
    fn from(raw: Vec<String>) -> Self {
        let w = raw[0].len();
        let h = raw.len();
        let d: Vec<char> = raw.iter().flat_map(|l| l.chars()).collect();
        Self {
            width: w,
            height: h,
            data: d,
        }
    }

    fn get(&self, i: usize, j: usize) -> Option<&char> {
        self.data.get(i * self.width + j)
    }

    fn get_mut(&mut self, i: usize, j: usize) -> Option<&mut char> {
        self.data.get_mut(i * self.width + j)
    }

    fn under(&self, cart: &Cart) -> Option<&char> {
        self.get(cart.i, cart.j)
    }

    fn under_mut(&mut self, cart: &Cart) -> Option<&mut char> {
        self.get_mut(cart.i, cart.j)
    }

    fn index(&self, a: usize) -> (usize, usize) {
        (a / self.width, a % self.width)
    }

    fn find_carts(&self) -> Vec<Cart> {
        self.data
            .iter()
            .enumerate()
            .filter_map(|(n, t)| {
                if ['<', '>', '^', 'v'].contains(t) {
                    let (i, j) = self.index(n);
                    Some(Cart::from(i, j, t))
                } else {
                    None
                }
            })
            .collect()
    }

    // fn detect(&self, cart: &Cart) -> char {
    //     let i = cart.i;
    //     let j = cart.j;
    //     if i > 0
    //         && j > 0
    //         && (self.get(i, j - 1) == Some(&'-') || self.get(i, j - 1) == Some(&'>'))
    //         && (self.get(i, j + 1) == Some(&'-') || self.get(i, j + 1) == Some(&'<'))
    //         && (self.get(i - 1, j) == Some(&'|') || self.get(i - 1, j) == Some(&'v'))
    //         && (self.get(i + 1, j) == Some(&'|') || self.get(i + 1, j) == Some(&'^'))
    //     {
    //         '+'
    //     } else if j > 1
    //         && (self.get(i, j - 1) == Some(&'-') || self.get(i, j - 1) == Some(&'>'))
    //         && (self.get(i, j + 1) == Some(&'-') || self.get(i, j + 1) == Some(&'<'))
    //     {
    //         '-'
    //     } else if i > 1
    //         && (self.get(i - 1, j) == Some(&'|') || self.get(i - 1, j) == Some(&'v'))
    //         && (self.get(i + 1, j) == Some(&'|') || self.get(i + 1, j) == Some(&'^'))
    //     {
    //         '|'
    //     } else {
    //         panic!();
    //     }
    // }
}

impl std::fmt::Display for Track {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let mut out: String = self.data.iter().collect();
        for i in (self.width..(self.width + 1) * self.height).step_by(self.width + 1) {
            out.insert(i, '\n');
        }
        write!(f, "{}", out)
    }
}

fn runb(filename: &str, print: bool) -> (usize, usize) {
    let raw = common::read_in(filename);
    let mut track = Track::from(raw);
    let mut carts = track.find_carts();
    if print {
        println!("{}", track);
    }
    for i in 0..6 {
        // println!("{:?}", carts);
        carts.sort_unstable();
        for cart in &mut carts {
            match track.under(cart) {
                Some('X') => {
                    cart.on = 'X';
                }
                Some('>') => {
                    *track.under_mut(cart).unwrap() = cart.on;
                    cart.j += 1;
                    *track.under_mut(cart).unwrap() = match track.under(cart) {
                        Some('-') => {
                            cart.on = '-';
                            '>'
                        }
                        Some('\\') => {
                            cart.on = '\\';
                            'v'
                        }
                        Some('/') => {
                            cart.on = '/';
                            '^'
                        }
                        Some('+') => {
                            cart.on = '+';
                            match cart.corner() {
                                Dir::Left => '^',
                                Dir::Straight => '>',
                                Dir::Right => 'v',
                            }
                        }
                        _ => {
                            cart.on = 'X';
                            'X'
                        }
                    };
                }
                Some('<') => {
                    *track.under_mut(cart).unwrap() = cart.on;
                    cart.j -= 1;
                    *track.under_mut(cart).unwrap() = match track.under(cart) {
                        Some('-') => {
                            cart.on = '-';
                            '<'
                        }
                        Some('\\') => {
                            cart.on = '\\';
                            '^'
                        }
                        Some('/') => {
                            cart.on = '/';
                            'v'
                        }
                        Some('+') => {
                            cart.on = '+';
                            match cart.corner() {
                                Dir::Left => 'v',
                                Dir::Straight => '<',
                                Dir::Right => '^',
                            }
                        }
                        _ => {
                            cart.on = 'X';
                            'X'
                        }
                    }
                }
                Some('v') => {
                    *track.under_mut(cart).unwrap() = cart.on;
                    cart.i += 1;
                    *track.under_mut(cart).unwrap() = match track.under(cart) {
                        Some('|') => {
                            cart.on = '|';
                            'v'
                        }
                        Some('/') => {
                            cart.on = '/';
                            '<'
                        }
                        Some('\\') => {
                            cart.on = '\\';
                            '>'
                        }
                        Some('+') => {
                            cart.on = '+';
                            match cart.corner() {
                                Dir::Left => '>',
                                Dir::Straight => 'v',
                                Dir::Right => '<',
                            }
                        }
                        _ => {
                            cart.on = 'X';
                            'X'
                        }
                    }
                }
                Some('^') => {
                    *track.under_mut(cart).unwrap() = cart.on;
                    cart.i -= 1;
                    *track.under_mut(cart).unwrap() = match track.under(cart) {
                        Some('|') => {
                            cart.on = '|';
                            '^'
                        }
                        Some('\\') => {
                            cart.on = '\\';
                            '<'
                        }
                        Some('/') => {
                            cart.on = '/';
                            '>'
                        }
                        Some('+') => {
                            cart.on = '+';
                            match cart.corner() {
                                Dir::Left => '<',
                                Dir::Straight => '^',
                                Dir::Right => '>',
                            }
                        }
                        _ => {
                            cart.on = 'X';
                            'X'
                        }
                    }
                }
                _ => (),
            }
        }
        for cart in &mut carts {
            if cart.on == 'X' {
                let other = carts.iter().find(|c| c.i == cart.i && c.j == cart.j).unwrap();
                *track.under_mut(cart).unwrap() = other.on;
                // other.on = 'X';
            }
        }
        if print {
            println!("{}", track);
        }
        carts.retain(|c| c.on != 'X');
        if carts.len() == 1 {
            if !print {
                println!("{}", track);
            }
            return (carts[0].i, carts[0].j);
        }
    }
    (0,0)
}

fn run(filename: &str, print: bool) -> (usize, usize) {
    let raw = common::read_in(filename);
    let mut track = Track::from(raw);
    let mut carts = track.find_carts();
    if print {
        println!("{}", track);
    }
    loop {
        carts.sort_unstable();
        for cart in &mut carts {
            match track.under(cart) {
                Some('>') => {
                    *track.under_mut(cart).unwrap() = cart.on;
                    cart.j += 1;
                    *track.under_mut(cart).unwrap() = match track.under(cart) {
                        Some('-') => {
                            cart.on = '-';
                            '>'
                        }
                        Some('\\') => {
                            cart.on = '\\';
                            'v'
                        }
                        Some('/') => {
                            cart.on = '/';
                            '^'
                        }
                        Some('+') => {
                            cart.on = '+';
                            match cart.corner() {
                                Dir::Left => '^',
                                Dir::Straight => '>',
                                Dir::Right => 'v',
                            }
                        }
                        Some('<') => 'X',
                        _ => ' ',
                    };
                }
                Some('<') => {
                    *track.under_mut(cart).unwrap() = cart.on;
                    cart.j -= 1;
                    *track.under_mut(cart).unwrap() = match track.under(cart) {
                        Some('-') => {
                            cart.on = '-';
                            '<'
                        }
                        Some('\\') => {
                            cart.on = '\\';
                            '^'
                        }
                        Some('/') => {
                            cart.on = '/';
                            'v'
                        }
                        Some('+') => {
                            cart.on = '+';
                            match cart.corner() {
                                Dir::Left => 'v',
                                Dir::Straight => '<',
                                Dir::Right => '^',
                            }
                        }
                        Some('>') => 'X',
                        _ => ' ',
                    }
                }
                Some('v') => {
                    *track.under_mut(cart).unwrap() = cart.on;
                    cart.i += 1;
                    *track.under_mut(cart).unwrap() = match track.under(cart) {
                        Some('|') => {
                            cart.on = '|';
                            'v'
                        }
                        Some('/') => {
                            cart.on = '/';
                            '<'
                        }
                        Some('\\') => {
                            cart.on = '\\';
                            '>'
                        }
                        Some('+') => {
                            cart.on = '+';
                            match cart.corner() {
                                Dir::Left => '>',
                                Dir::Straight => 'v',
                                Dir::Right => '<',
                            }
                        }
                        Some('^') => 'X',
                        _ => ' ',
                    }
                }
                Some('^') => {
                    *track.under_mut(cart).unwrap() = cart.on;
                    cart.i -= 1;
                    *track.under_mut(cart).unwrap() = match track.under(cart) {
                        Some('|') => {
                            cart.on = '|';
                            '^'
                        }
                        Some('\\') => {
                            cart.on = '\\';
                            '<'
                        }
                        Some('/') => {
                            cart.on = '/';
                            '>'
                        }
                        Some('+') => {
                            cart.on = '+';
                            match cart.corner() {
                                Dir::Left => '<',
                                Dir::Straight => '^',
                                Dir::Right => '>',
                            }
                        }
                        Some('v') => 'X',
                        _ => ' ',
                    }
                }
                _ => (),
            }
            if track.under(cart) == Some(&'X') {
                if print {
                    println!("{}", track);
                }
                return (cart.i, cart.j);
            }
        }
        if print {
            println!("{}", track);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_a() {
        let (y, x) = run("input/test0", false);
        assert_eq!("7,3", format!("{},{}", x, y));
    }

    #[test]
    fn test_a0() {
        let (y, x) = run("input/input", false);
        assert_eq!("53,133", format!("{},{}", x, y));
    }
}

fn main() {
    // let (y, x) = run("input/test0", true);
    // println!("{},{}", x, y);

    // let (y, x) = run("input/input", false);
    // println!("{},{}", x, y);

    let (y, x) = runb("input/test1", true);
    println!("{},{}", x, y);

    // let (y, x) = runb("input/input", false);
    // println!("{},{}", x, y);
}
