extern crate common;

use common::read_in;
use std::collections::HashSet;

fn checksum(data: Vec<String>) -> u32 {
    let mut triples = 0;
    let mut doubles = 0;

    for word in data {
        let mut double = false;
        let mut triple = false;
        let set: HashSet<char> = word.chars().collect();
        for letter in set {
            let matches = word.matches(letter).count();
            if matches == 2 {
                double = true;
            }
            if matches == 3 {
                triple = true;
            }
        }
        if double {
            doubles += 1;
        }
        if triple {
            triples += 1;
        }
    }
    doubles * triples
}

fn id(data: Vec<String>) -> Option<String> {
    for i in 0..data.len() {
        for j in (i + 1)..data.len() {
            let mut diff = 0;
            for k in 0..data[0].chars().count() {
                if data[i].as_bytes()[k] != data[j].as_bytes()[k] {
                    diff += 1;
                }
            }
            if diff == 1 {
                for k in 0..data[0].chars().count() {
                    if data[i].as_bytes()[k] != data[j].as_bytes()[k] {
                        let mut res = data[i].clone();
                        res.remove(k);
                        return Some(res);
                    }
                }
            }
        }
    }
    None
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_a() {
        assert_eq!(12, checksum(read_in("input/test0")));
    }
    #[test]
    fn test_b() {
        let data = read_in("input/test1");
        let ans = id(data).expect("No boxes have IDs which differ by exactly one character");
        assert_eq!("fgij", ans);
    }
}

fn main() {
    let data = read_in("input/input");
    println!("Checksum: {}", checksum(data.clone()));

    let ans = id(data).expect("No boxes have IDs which differ by exactly one character");
    println!("Common letters: {}", ans);
}
