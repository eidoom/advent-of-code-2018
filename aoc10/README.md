# [10](https://gitlab.com/eidoom/advent-of-code-2018/-/tree/master/aoc10)
* [Question](https://adventofcode.com/2018/day/10)
## Concepts
* Using [modules](https://doc.rust-lang.org/reference/items/modules.html) and [`use`](https://doc.rust-lang.org/reference/items/use-declarations.html) to split up code into different files
* Being aware of the size of objects
    * Assuming the `char`s are encoded with one byte, trying to create a `Vec<char>` buffer of the sky for int `input`'s first tick requires over 11[GB](https://en.wikipedia.org/wiki/Gigabyte)s
* More `regex`
* Delays/waits with [`sleep`](https://doc.rust-lang.org/std/thread/fn.sleep.html)
* Generics
* Implementing basic operators like [`Add`](https://doc.rust-lang.org/std/ops/trait.Add.html) (`+`) and [`AddAssign`](https://doc.rust-lang.org/std/ops/trait.AddAssign.html) (`+=`) as `trait`s for user-defined `struct`s
