use super::point::Point;

#[derive(Debug)]
pub struct Points {
    pub data: Vec<Point>,
}

impl Points {
    pub fn new() -> Self {
        Self {
            data: Vec::<Point>::new(),
        }
    }

    pub fn to_string(&self) -> Option<String> {
        let y0 = self.data.iter().map(|p| p.q.y).min().unwrap();
        let y1 = self.data.iter().map(|p| p.q.y).max().unwrap();
        let h = (y1 - y0 + 1) as usize;

        if h < 11 {
            let x0 = self.data.iter().map(|p| p.q.x).min().unwrap();
            let x1 = self.data.iter().map(|p| p.q.x).max().unwrap();
            let w = (x1 - x0 + 1) as usize;

            let mut sky = vec![' '; (w + 1) * h];

            for point in &self.data {
                let x = (point.q.x - x0) as usize;
                let y = (point.q.y - y0) as usize;
                sky[(w + 1) * y + x] = '#';
            }

            for y in 0..h {
                sky[(w + 1) * y + w] = '\n';
            }

            Some(sky.iter().collect())
        } else {
            None
        }
    }

    pub fn tick(&mut self) {
        self.data.iter_mut().for_each(|p| p.tick());
    }
}
