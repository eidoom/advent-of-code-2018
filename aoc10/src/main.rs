extern crate regex;

extern crate common;

mod coord;
mod point;
mod points;

use crate::coord::Coord;
use crate::point::Point;
use crate::points::Points;

fn yank(line: &str, mat: &regex::Captures, i: usize) -> isize {
    let cap = mat.get(i).unwrap();
    let val = (&line[cap.start()..cap.end()]).parse::<isize>().unwrap();
    val
}

fn run(filename: &str) {
    let data = common::read_in(filename);
    let re = regex::Regex::new(
        r"^position=< ?(-?\d+), {1,2}(-?\d+)> velocity= ?< ?(-?\d+), {1,2}(-?\d+)>$",
    )
    .unwrap();

    let mut points = Points::new();
    for line in data {
        let mat = re.captures(&line).unwrap();
        let position = Coord {
            x: yank(&line, &mat, 1),
            y: yank(&line, &mat, 2),
        };
        let velocity = Coord {
            x: yank(&line, &mat, 3),
            y: yank(&line, &mat, 4),
        };
        let point = Point::from(position, velocity);
        points.data.push(point);
    }

    let mut i = 0;
    loop {
        match points.to_string() {
            Some(s) => {
                println!("{}", s);
                println!("{}", i);
                std::thread::sleep(std::time::Duration::from_millis(1000));
            }
            None => (),
        }
        points.tick();
        i += 1;
    }
}

// #[cfg(test)]
// mod tests {
//     use super::*;

//     #[test]
//     fn test_a() {
//         run("input/test0");
//         assert_eq!(1, 1);
//     }
// }

fn main() {
    // run("input/test0");
    run("input/input");
}
