use super::coord::Coord;

#[derive(Debug)]
pub struct Point {
    pub q: Coord<isize>,
    v: Coord<isize>,
}

impl Point {
    pub fn from(q_: Coord<isize>, v_: Coord<isize>) -> Self {
        Self { q: q_, v: v_ }
    }

    pub fn tick(&mut self) {
        self.q += self.v;
    }
}
