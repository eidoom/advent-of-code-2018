extern crate common;

use std::time::Instant;

fn fuel_cell_power(srl_no: isize, x: usize, y: usize) -> isize {
    let rk_id = (x as isize) + 10;
    let mut pwr_lvl = rk_id * (y as isize);
    pwr_lvl += srl_no;
    pwr_lvl *= rk_id;
    pwr_lvl /= 100;
    pwr_lvl %= 10;
    pwr_lvl -= 5;
    pwr_lvl
}

#[derive(Debug, PartialEq)]
struct Answer {
    x: usize,
    y: usize,
    v: isize,
}

impl Answer {
    fn new() -> Self {
        Self { x: 0, y: 0, v: 0 }
    }

    fn from(x_: usize, y_: usize, v_: isize) -> Self {
        Self {
            x: x_,
            y: y_,
            v: v_,
        }
    }
}

#[derive(Debug, PartialEq)]
struct Answer2 {
    x: usize,
    y: usize,
    s: usize,
    v: isize,
}

impl Answer2 {
    fn new() -> Self {
        Self {
            x: 0,
            y: 0,
            s: 0,
            v: 0,
        }
    }

    fn from(x_: usize, y_: usize, s_: usize, v_: isize) -> Self {
        Self {
            x: x_,
            y: y_,
            s: s_,
            v: v_,
        }
    }
}

fn search(srl_no: isize) -> Answer {
    const SIZE: usize = 300;
    let mut grid = [[0; SIZE]; SIZE];
    for x in 0..SIZE {
        for y in 0..SIZE {
            grid[y][x] = fuel_cell_power(srl_no, x, y);
        }
    }
    let mut largest = Answer::new();
    for x in 0..SIZE - 2 {
        for y in 0..SIZE - 2 {
            let mut current = 0;
            for xx in 0..3 {
                for yy in 0..3 {
                    current += grid[y + yy][x + xx];
                }
            }
            if current > largest.v {
                largest = Answer::from(x, y, current);
            }
        }
    }
    largest
}

fn search2(srl_no: isize) -> Answer2 {
    const SIZE: usize = 300;
    let mut grid = [0; SIZE * SIZE];
    for x in 0..SIZE {
        for y in 0..SIZE {
            grid[y * SIZE + x] = fuel_cell_power(srl_no, x, y);
        }
    }
    let mut largest = Answer2::new();
    let mut cache = [0; SIZE * SIZE];
    for size in 1..=SIZE {
        for x in 0..=(SIZE - size) {
            for y in 0..=(SIZE - size) {
                for xx in 0..size {
                    cache[y * SIZE + x] += grid[(y + size - 1) * SIZE + x + xx];
                }
                for yy in 0..(size - 1) {
                    cache[y * SIZE + x] += grid[(y + yy) * SIZE + x + size - 1];
                }
                if cache[y * SIZE + x] > largest.v {
                    largest = Answer2::from(x, y, size, cache[y * SIZE + x]);
                }
            }
        }
    }
    largest
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_a1() {
        assert_eq!(4, fuel_cell_power(8, 3, 5));
    }

    #[test]
    fn test_a2() {
        assert_eq!(-5, fuel_cell_power(57, 122, 79));
    }

    #[test]
    fn test_a3() {
        assert_eq!(0, fuel_cell_power(39, 217, 196));
    }

    #[test]
    fn test_a4() {
        assert_eq!(4, fuel_cell_power(71, 101, 153));
    }

    #[test]
    fn test_a5() {
        let ans = search(18);
        assert_eq!(
            ans,
            Answer {
                x: 33,
                y: 45,
                v: 29,
            }
        );
    }

    #[test]
    fn test_a6() {
        let ans = search(42);
        assert_eq!(
            ans,
            Answer {
                x: 21,
                y: 61,
                v: 30,
            }
        );
    }

    #[test]
    fn test_a0() {
        let ans = search(1955);
        assert_eq!(format!("{},{}", ans.x, ans.y), "21,93");
    }

    #[test]
    fn test_b1() {
        let ans = search2(18);
        assert_eq!(
            Answer2 {
                x: 90,
                y: 269,
                s: 16,
                v: 113,
            },
            ans
        );
    }
    #[test]
    fn test_b2() {
        let ans = search2(42);
        assert_eq!(
            Answer2 {
                x: 232,
                y: 251,
                s: 12,
                v: 119,
            },
            ans
        );
    }
}

fn main() {
    let now = Instant::now();
    let larger = search(1955);
    let elapsed = now.elapsed();
    println!("{},{} ({:?})", larger.x, larger.y, elapsed);

    let now = Instant::now();
    let largest = search2(1955);
    let elapsed = now.elapsed();
    println!("{},{},{} ({:?})", largest.x, largest.y, largest.s, elapsed);
}
