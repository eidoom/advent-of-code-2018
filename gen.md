```zsh
vi main.rs.tmp
```

```vim
extern crate common;

fn run(filename: &str) {
    let data = common::read_in(filename);
    println!("Hello, world!\n{:?}", data);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_a() {
        run("input/test0");
        assert_eq!(1, 1);
    }
}

fn main() {
    run("input/input");
}
```

```zsh
for x in {01..25}; do 
    cargo init --bin aoc$x; 
    echo 'common={path="../common"}' >> aoc$x/Cargo.toml; 
    cp main.rs.tmp aoc$x/src/main.rs;
    touch aoc$x/input/input;
    touch aoc$x/input/test0;
done
```

```zsh
vi README.md.tmp
```

```vim
# [A](https://gitlab.com/eidoom/advent-of-code-2018/-/tree/master/aocBA)
* [Question](https://adventofcode.com/2018/day/A)
## Concepts
```

```zsh
for i in {1..9}; do perl -p -e "s|A|$i|g;" -e "s|B|0|g;" README.md.tmp > aoc0$i/README.md; done
for i in {10..25}; do perl -p -e "s|A|$i|g;" -e "s|B||g;" README.md.tmp > aoc$i/README.md; done
```
