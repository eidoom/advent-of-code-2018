fn run(start: &str, done: usize, predict: usize) -> String {
    let mut data: Vec<usize> = start
        .chars()
        .map(|c| c.to_digit(10).unwrap() as usize)
        .collect();
    let mut elf1 = 0;
    let mut elf2 = 1;
    let end = done + predict;
    while data.len() < end {
        let new = data[elf1] + data[elf2];
        if new > 9 {
            data.push(new / 10);
            data.push(new % 10);
        } else {
            data.push(new);
        }
        elf1 = (elf1 + data[elf1] + 1) % data.len();
        elf2 = (elf2 + data[elf2] + 1) % data.len();

        // println!("{:?}", data);
    }
    data[done..end].iter().map(|n| n.to_string()).collect()
}

fn runb(start: &str, mtch: &str) -> usize {
    let mut data: Vec<usize> = start
        .chars()
        .map(|c| c.to_digit(10).unwrap() as usize)
        .collect();
    let l = mtch.len();
    let cond: Vec<usize> = mtch
        .chars()
        .map(|c| c.to_digit(10).unwrap() as usize)
        .collect();
    let mut elf1 = 0;
    let mut elf2 = 1;
    loop {
        let new = data[elf1] + data[elf2];
        if new > 9 {
            data.push(new / 10);
            data.push(new % 10);
        } else {
            data.push(new);
        }
        elf1 = (elf1 + data[elf1] + 1) % data.len();
        elf2 = (elf2 + data[elf2] + 1) % data.len();

        if data.len() > l {
            if &data[data.len() - l..] == cond {
                return data.len() - l;
            }
            if &data[data.len() - l - 1..data.len() - 1] == cond {
                return data.len() - l - 1;
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_a1() {
        assert_eq!(run("37", 9, 10), "5158916779");
    }

    #[test]
    fn test_a2() {
        assert_eq!(run("37", 5, 10), "0124515891");
    }

    #[test]
    fn test_a3() {
        assert_eq!(run("37", 18, 10), "9251071085");
    }

    #[test]
    fn test_a4() {
        assert_eq!(run("37", 2018, 10), "5941429882");
    }

    #[test]
    fn test_b1() {
        assert_eq!(runb("37", "51589"), 9);
    }

    #[test]
    fn test_b2() {
        assert_eq!(runb("37", "01245"), 5);
    }

    #[test]
    fn test_b3() {
        assert_eq!(runb("37", "92510"), 18);
    }

    #[test]
    fn test_b4() {
        assert_eq!(runb("37", "59414"), 2018);
    }
}

fn main() {
    println!("{:?}", run("37", 260321, 10));
    println!("{:?}", runb("37", "260321"));
}
