extern crate common;

use std::collections::VecDeque;
use std::time::Instant;

fn parse(filename: &str) -> (usize, usize) {
    let raw = common::read_in_single(filename);
    let data: Vec<&str> = raw.split(" ").collect();
    let players = data[0].parse::<usize>().unwrap();
    let last = data[6].parse::<usize>().unwrap();
    (players, last)
}

fn run(players: usize, last: usize) -> usize {
    let mut scores = vec![0; players];
    let size = (last + 1) - last / 23;
    let mut marbles = VecDeque::<usize>::with_capacity(size);
    marbles.push_back(0);
    marbles.push_back(1);
    let mut j = 0;
    for i in 2..=last {
        if i % 23 == 0 {
            marbles.rotate_left(7);
            scores[j] += i + marbles.pop_back().unwrap();
        } else {
            marbles.rotate_right(2);
            marbles.push_back(i);
        }
        j = (j + 1) % players;
    }
    *scores.iter().max().unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_a0() {
        let (p, l) = parse("input/test0");
        let res = run(p, l);
        assert_eq!(32, res);
    }

    #[test]
    fn test_a1() {
        let (p, l) = parse("input/test1");
        let res = run(p, l);
        assert_eq!(8317, res);
    }

    #[test]
    fn test_a2() {
        let (p, l) = parse("input/test2");
        let res = run(p, l);
        assert_eq!(146373, res);
    }

    #[test]
    fn test_a3() {
        let (p, l) = parse("input/test3");
        let res = run(p, l);
        assert_eq!(2764, res);
    }

    #[test]
    fn test_a4() {
        let (p, l) = parse("input/test4");
        let res = run(p, l);
        assert_eq!(54718, res);
    }

    #[test]
    fn test_a5() {
        let (p, l) = parse("input/test5");
        let res = run(p, l);
        assert_eq!(37305, res);
    }

    #[test]
    fn test_a() {
        let (p, l) = parse("input/input");
        let res = run(p, l);
        assert_eq!(405143, res);
    }
}

fn main() {
    let (p, l) = parse("input/input");

    let now = Instant::now();
    let res_a = run(p, l);
    let elapsed = now.elapsed();
    println!("{} ({:?})", res_a, elapsed);

    let now = Instant::now();
    let res_b = run(p, 100 * l);
    let elapsed = now.elapsed();
    println!("{} ({:?})", res_b, elapsed);
}
