# [9](https://gitlab.com/eidoom/advent-of-code-2018/-/tree/master/aoc09)
* [Question](https://adventofcode.com/2018/day/9)
## Concepts
* Profiling with [flamegraph](https://github.com/flamegraph-rs/flamegraph)
    * Run as `RUSTFLAGS='-g' cargo flamegraph`
* Rust's [linked lists](https://doc.rust-lang.org/std/collections/struct.LinkedList.html) don't have insertions, but [this crate](https://crates.io/crates/linked-list) does
    * Didn't end up using it because it was slower than Vec-based
* Tried to implement a data structure of a doubly-linked list
    * with periodic boundaries (circular)?
    * using [Rc](https://doc.rust-lang.org/std/rc/struct.Rc.html) from [rc](https://doc.rust-lang.org/std/rc/index.html)
    * and [RefCell](https://doc.rust-lang.org/std/cell/struct.RefCell.html) from [cell](https://doc.rust-lang.org/std/cell/index.html)
    * cf [this](https://gist.github.com/matey-jack/3e19b6370c6f7036a9119b79a82098ca)
    * but then discovered you can [rotate](https://doc.rust-lang.org/std/collections/struct.VecDeque.html#method.rotate_left) VecDeques for arbitrary insertions
* [VecDeque](https://doc.rust-lang.org/std/collections/struct.VecDeque.html) to the rescue
