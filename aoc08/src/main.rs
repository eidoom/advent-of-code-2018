extern crate common;

use std::collections::HashMap;
use std::time::Instant;

fn get_licence(filename: &str) -> Vec<usize> {
    let raw = common::read_in_single(filename);
    raw.split(" ")
        .map(|l| l.parse::<usize>().unwrap())
        .collect()
}

fn visit_a(data: &mut std::slice::Iter<usize>, sum: &mut usize) {
    let num_child = *data.next().unwrap();
    let num_meta = *data.next().unwrap();
    for _ in 0..num_child {
        visit_a(data, sum);
    }
    for _ in 0..num_meta {
        *sum += data.next().unwrap();
    }
}

fn run_a(data: &Vec<usize>) -> usize {
    let mut sum = 0;
    visit_a(&mut data.iter(), &mut sum);
    sum
}

#[derive(Clone, Debug)]
struct Node {
    children: Vec<usize>,
    value: usize,
}

impl Node {
    fn new() -> Self {
        Self {
            children: Vec::new(),
            value: 0,
        }
    }
}

type Nodes = HashMap<usize, Node>;

fn visit_b(data: &mut std::slice::Iter<usize>, nodes: &mut Nodes, i: &mut usize, j: &mut usize) {
    let num_child = *data.next().unwrap();
    let num_meta = *data.next().unwrap();
    let index = *j;
    *j += 1;
    let mut node = Node::new();

    if num_child == 0 {
        for _ in 0..num_meta {
            node.value += data.next().unwrap();
        }
    } else {
        for _ in 0..num_child {
            *i += 1;
            node.children.push(*i);
            visit_b(data, nodes, i, j);
        }

        let init = nodes.clone();
        for _ in 0..num_meta {
            match node.children.get(data.next().unwrap() - 1) {
                Some(n) => {
                    node.value += init[n].value;
                }
                None => (),
            }
        }
    }

    nodes.insert(index, node);
}

fn run_b(data: &Vec<usize>) -> usize {
    let mut nodes = Nodes::new();
    visit_b(&mut data.iter(), &mut nodes, &mut 0, &mut 0);
    nodes[&0].value
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_a0() {
        assert_eq!(138, run_a(&get_licence("input/test0")));
    }

    #[test]
    fn test_a() {
        assert_eq!(49180, run_a(&get_licence("input/input")));
    }

    #[test]
    fn test_b0() {
        assert_eq!(66, run_b(&get_licence("input/test0")));
    }

    #[test]
    fn test_b1() {
        assert_eq!(31, run_b(&get_licence("input/test1")));
    }
}

fn main() {
    let data = get_licence("input/input");

    let now = Instant::now();
    let ans_a = run_a(&data);
    let elapsed = now.elapsed();
    println!("{} ({:?})", ans_a, elapsed);

    let now = Instant::now();
    let ans_b = run_b(&data);
    let elapsed = now.elapsed();
    println!("{} ({:?})", ans_b, elapsed);
}
