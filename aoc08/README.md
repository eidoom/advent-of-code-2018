# [8](https://gitlab.com/eidoom/advent-of-code-2018/-/tree/master/aoc08)
* [Question](https://adventofcode.com/2018/day/8)
## Concepts
* Timing with [`std::time::Instant`](https://doc.rust-lang.org/std/time/struct.Instant.html)
* Recursive DFS
* Cloning to avoid multiple mutable references
* [Iterators](https://doc.rust-lang.org/std/iter/index.html)
