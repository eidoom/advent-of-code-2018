# [16](https://gitlab.com/eidoom/advent-of-code-2018/-/tree/master/aoc16)
* [Question](https://adventofcode.com/2018/day/16)
## Concepts
* Nightly
    * Install
        ```shell
        rustup install nightly
        ```
    * To use this toolchain, use `cargo +nightly`
    * Benchmarks
        ```shell
        cargo +nightly bench
        ```
