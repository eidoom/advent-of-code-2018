#![feature(test)]

extern crate test;

extern crate common;

use std::collections::HashSet;

fn read_nums(nums: &str, delimiter: &str) -> Vec<usize> {
    nums.split(delimiter)
        .map(|e| e.parse::<usize>().unwrap())
        .collect()
}

fn read_line(line: &str) -> Vec<usize> {
    read_nums(&line[9..line.len() - 1], ", ")
}

fn read_sample(raw: &str) -> [Vec<usize>; 3] {
    let data: Vec<&str> = raw.split("\n").collect();
    let before = read_line(&data[0]);
    let instruction = read_nums(data[1], " ");
    let after = read_line(&data[2]);
    [before, instruction, after]
}

fn read_file(filename: &str) -> (Vec<[Vec<usize>; 3]>, Vec<Vec<usize>>) {
    let raw = common::read_in_whole(filename);
    let data: Vec<&str> = raw.split("\n\n\n\n").collect();
    let samples: Vec<[Vec<usize>; 3]> = data[0].split("\n\n").map(|l| read_sample(l)).collect();
    let test_prog: Vec<Vec<usize>> = data[1].split("\n").map(|l| read_nums(l, " ")).collect();
    (samples, test_prog)
}

fn test_device(sample: &[Vec<usize>; 3]) -> usize {
    let (pre, ins, post) = (&sample[0], &sample[1], &sample[2]);
    let (a, b, c) = (ins[1], ins[2], ins[3]);
    let mut count: usize = 0;
    // addr
    if post[c] == pre[a] + pre[b] {
        count += 1;
    }
    // addi
    if post[c] == pre[a] + b {
        count += 1;
    }
    // mulr
    if post[c] == pre[a] * pre[b] {
        count += 1;
    }
    // muli
    if post[c] == pre[a] * b {
        count += 1;
    }
    // banr
    if post[c] == pre[a] & pre[b] {
        count += 1;
    }
    // bani
    if post[c] == pre[a] & b {
        count += 1;
    }
    // borr
    if post[c] == pre[a] | pre[b] {
        count += 1;
    }
    // bori
    if post[c] == pre[a] | b {
        count += 1;
    }
    // setr
    if post[c] == pre[a] {
        count += 1;
    }
    // seti
    if post[c] == a {
        count += 1;
    }
    // gtir
    if post[c] == if a > pre[b] { 1 } else { 0 } {
        count += 1;
    }
    // gtri
    if post[c] == if pre[a] > b { 1 } else { 0 } {
        count += 1;
    }
    // gtrr
    if post[c] == if pre[a] > pre[b] { 1 } else { 0 } {
        count += 1;
    }
    // eqir
    if post[c] == if a == pre[b] { 1 } else { 0 } {
        count += 1;
    }
    // eqri
    if post[c] == if pre[a] == b { 1 } else { 0 } {
        count += 1;
    }
    // eqrr
    if post[c] == if pre[a] == pre[b] { 1 } else { 0 } {
        count += 1;
    }
    count
}

#[derive(PartialEq, Eq, Hash, Debug, Clone, Copy)]
enum Opcode {
    Addr,
    Addi,
    Mulr,
    Muli,
    Banr,
    Bani,
    Borr,
    Bori,
    Setr,
    Seti,
    Gtir,
    Gtri,
    Gtrr,
    Eqir,
    Eqri,
    Eqrr,
}

impl Default for Opcode {
    fn default() -> Self {
        Self::Addr
    }
}

fn decode_device(sample: &[Vec<usize>; 3]) -> Vec<Opcode> {
    let (pre, ins, post): (&Vec<usize>, &Vec<usize>, &Vec<usize>) =
        (&sample[0], &sample[1], &sample[2]);
    let (a, b, c): (usize, usize, usize) = (ins[1], ins[2], ins[3]);
    let mut matches = Vec::<Opcode>::new();
    if post[c] == pre[a] + pre[b] {
        matches.push(Opcode::Addr);
    }
    if post[c] == pre[a] + b {
        matches.push(Opcode::Addi);
    }
    if post[c] == pre[a] * pre[b] {
        matches.push(Opcode::Mulr);
    }
    if post[c] == pre[a] * b {
        matches.push(Opcode::Muli);
    }
    if post[c] == pre[a] & pre[b] {
        matches.push(Opcode::Banr);
    }
    if post[c] == pre[a] & b {
        matches.push(Opcode::Bani);
    }
    if post[c] == pre[a] | pre[b] {
        matches.push(Opcode::Borr);
    }
    if post[c] == pre[a] | b {
        matches.push(Opcode::Bori);
    }
    if post[c] == pre[a] {
        matches.push(Opcode::Setr);
    }
    if post[c] == a {
        matches.push(Opcode::Seti);
    }
    if post[c] == if a > pre[b] { 1 } else { 0 } {
        matches.push(Opcode::Gtir);
    }
    if post[c] == if pre[a] > b { 1 } else { 0 } {
        matches.push(Opcode::Gtri);
    }
    if post[c] == if pre[a] > pre[b] { 1 } else { 0 } {
        matches.push(Opcode::Gtrr);
    }
    if post[c] == if a == pre[b] { 1 } else { 0 } {
        matches.push(Opcode::Eqir);
    }
    if post[c] == if pre[a] == b { 1 } else { 0 } {
        matches.push(Opcode::Eqri);
    }
    if post[c] == if pre[a] == pre[b] { 1 } else { 0 } {
        matches.push(Opcode::Eqrr);
    }
    matches
}

fn run_device(reg: &mut [usize; 4], opcode: Opcode, a: usize, b: usize, c: usize) {
    reg[c] = match opcode {
        Opcode::Addr => reg[a] + reg[b],
        Opcode::Addi => reg[a] + b,
        Opcode::Mulr => reg[a] * reg[b],
        Opcode::Muli => reg[a] * b,
        Opcode::Banr => reg[a] & reg[b],
        Opcode::Bani => reg[a] & b,
        Opcode::Borr => reg[a] | reg[b],
        Opcode::Bori => reg[a] | b,
        Opcode::Setr => reg[a],
        Opcode::Seti => a,
        Opcode::Gtir => {
            if a > reg[b] {
                1
            } else {
                0
            }
        }
        Opcode::Gtri => {
            if reg[a] > b {
                1
            } else {
                0
            }
        }
        Opcode::Gtrr => {
            if reg[a] > reg[b] {
                1
            } else {
                0
            }
        }
        Opcode::Eqir => {
            if a == reg[b] {
                1
            } else {
                0
            }
        }
        Opcode::Eqri => {
            if reg[a] == b {
                1
            } else {
                0
            }
        }
        Opcode::Eqrr => {
            if reg[a] == reg[b] {
                1
            } else {
                0
            }
        }
    };
}

fn run_a(samples: &Vec<[Vec<usize>; 3]>) -> usize {
    samples
        .iter()
        .fold(0, |acc, x| if test_device(x) >= 3 { acc + 1 } else { acc })
}

fn run_b(samples: &Vec<[Vec<usize>; 3]>, test_prog: &Vec<Vec<usize>>) -> usize {
    let mut poss: [HashSet<Opcode>; 16] = Default::default();
    for sample in samples {
        let opcode = sample[1][0];
        let pos = decode_device(sample);
        pos.iter().for_each(|&name| {
            poss[opcode].insert(name);
        });
    }
    let mut res: [Opcode; 16] = Default::default();
    for _ in 0..16 {
        if let Some((i, s)) = poss.iter().enumerate().find(|(_, x)| x.len() == 1) {
            let opcode = *s.iter().nth(0).unwrap();
            res[i] = opcode;
            for pos in &mut poss {
                pos.remove(&opcode);
            }
        }
    }
    let mut reg: [usize; 4] = Default::default();
    test_prog.iter().for_each(|ins| {
        run_device(&mut reg, res[ins[0]], ins[1], ins[2], ins[3]);
    });
    reg[0]
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_a1() {
        let (samples, _) = read_file("input/test0");
        assert_eq!(run_a(&samples), 1);
    }

    #[test]
    fn test_a0() {
        let (samples, _) = read_file("input/input");
        assert_eq!(run_a(&samples), 642);
    }

    #[test]
    fn test_b() {
        let (samples, test_prog) = read_file("input/input");
        assert_eq!(run_b(&samples, &test_prog), 481);
    }

    #[bench]
    fn bench_a(b: &mut test::Bencher) {
        let (samples, _) = read_file("input/input");
        b.iter(|| run_a(&samples));
    }

    #[bench]
    fn bench_b(b: &mut test::Bencher) {
        let (samples, test_prog) = read_file("input/input");
        b.iter(|| run_b(&samples, &test_prog));
    }
}

fn main() {
    let (samples, test_prog) = read_file("input/input");
    let a = run_a(&samples);
    println!("{}", a);
    let b = run_b(&samples, &test_prog);
    println!("{}", b);
}
