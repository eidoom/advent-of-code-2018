# [advent-of-code-2018](https://gitlab.com/eidoom/advent-of-code-2018)

[Advent of Code 2018](https://adventofcode.com/2018/) in [Rust](https://www.rust-lang.org/).

* [Install](https://www.rust-lang.org/tools/install) Rust
    ```shell
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    ```

* Tests:
    ```shell
    cargo test
    ```

* Answer:
    ```shell
    cargo run --release
    ```

## Links

* [Solutions megathreads](https://old.reddit.com/r/adventofcode/wiki/solution_megathreads#wiki_december_2018)
