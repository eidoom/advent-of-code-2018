extern crate common;

fn diff(x: usize, y: usize) -> usize {
    if x > y {
        x - y
    } else {
        y - x
    }
}

fn start(filename: &str) -> (usize, usize, Vec<Vec<usize>>) {
    let raw = common::read_in(filename);

    let points: Vec<Vec<usize>> = raw
        .iter()
        .map(|y| {
            y.split(", ")
                .map(|x| x.parse::<usize>().expect("Couldn't parse integer input"))
                .collect::<Vec<usize>>()
        })
        .collect();

    let is: Vec<usize> = points.iter().map(|x| x[0]).collect();
    let js: Vec<usize> = points.iter().map(|x| x[1]).collect();

    let i_min = is.iter().min().expect("Empty vector has no min");
    let i_max = is.iter().max().expect("Empty vector has no max");
    let j_min = js.iter().min().expect("Empty vector has no min");
    let j_max = js.iter().max().expect("Empty vector has no max");

    // this could be calculated smartly by increasing the margin until the set of unique entries on
    // the boundary of "closests" becomes constant (for a) - but that would be expensive
    let margin = *vec![i_min, i_max, j_min, j_max]
        .iter()
        .min()
        .expect("Empty vector has no min");

    let height = i_max - i_min + 1 + 2 * margin;
    let width = j_max - j_min + 1 + 2 * margin;

    (width, height, points)
}

fn run_a(width: usize, height: usize, points: &Vec<Vec<usize>>) -> usize {
    let mut closest = vec![vec![0; width]; height];
    {
        let mut dists = vec![vec![1000; width]; height];
        for (k, point) in points.iter().enumerate() {
            for i in 0..height {
                for j in 0..width {
                    let d = diff(i, point[0]) + diff(j, point[1]);
                    if d < dists[i][j] {
                        dists[i][j] = d;
                        closest[i][j] = k + 1;
                    } else if d == dists[i][j] {
                        closest[i][j] = 0;
                    }
                }
            }
        }
    }

    let mut antipoints = Vec::<usize>::new();
    antipoints.append(&mut closest[0].clone());
    antipoints.append(&mut closest[closest.len() - 1].clone());
    for &j in vec![0, width - 1].iter() {
        for i in 0..height {
            antipoints.push(closest[i][j])
        }
    }
    antipoints.sort_unstable();
    antipoints.dedup();

    let mut max = 0;
    for x in (1..points.len() + 1).filter(|x| !antipoints.contains(x)) {
        let ans = closest.iter().flatten().filter(|&&y| y == x).count();
        if ans > max {
            max = ans;
        }
    }

    max
}

fn run_b(lim: usize, width: usize, height: usize, points: &Vec<Vec<usize>>) -> usize {
    let mut totals = vec![vec![0; width]; height];
    for point in points {
        for i in 0..height {
            for j in 0..width {
                totals[i][j] += diff(i, point[0]) + diff(j, point[1]);
            }
        }
    }

    totals.iter().flatten().filter(|&&x| x < lim).count()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_a() {
        let (w, h, ps) = start("input/test0");
        assert_eq!(17, run_a(w, h, &ps));
    }

    #[test]
    fn test_b() {
        let (w, h, ps) = start("input/test0");
        assert_eq!(16, run_b(32, w, h, &ps));
    }
}

fn main() {
    let (w, h, ps) = start("input/input");

    let ans = run_a(w, h, &ps);
    println!("{}", ans);

    let ans = run_b(10000, w, h, &ps);
    println!("{}", ans);
}
