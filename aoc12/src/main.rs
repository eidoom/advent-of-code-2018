extern crate common;

use std::collections::VecDeque;
use std::time::Instant;

fn parse(filename: &str) -> (VecDeque<bool>, Vec<usize>) {
    let data = common::read_in(filename);
    let rules: Vec<usize> = data[2..]
        .iter()
        .filter_map(|line| match line.chars().nth(9) {
            Some('#') => Some(
                line[..5]
                    .chars()
                    .enumerate()
                    .filter_map(|(i, c)| if c == '#' { Some(1 << i) } else { None })
                    .sum(),
            ),
            _ => None,
        })
        .collect();
    (data[0][15..].chars().map(|x| x == '#').collect(), rules)
}

fn evolve(mut cur_state: VecDeque<bool>, rules: Vec<usize>, num_gens: usize) -> isize {
    let mut start = 0;
    let mut old_sum = 0;
    let mut old_diff = 0;
    for k in 0..num_gens {
        match cur_state.iter().position(|&c| c) {
            Some(i) => {
                start += -2 + i as isize;
                for _ in i..4 {
                    cur_state.push_front(false);
                }
            }
            None => return 0,
        }

        match cur_state.iter().rposition(|&c| c) {
            Some(i) => {
                for _ in cur_state.len() - i - 1..4 {
                    cur_state.push_back(false);
                }
            }
            None => return 0,
        }

        let mut new_state = VecDeque::<bool>::with_capacity(cur_state.len());

        for i in 0..cur_state.len() - 4 {
            let cur = (0..5)
                .into_iter()
                .filter_map(|j| if cur_state[i + j] { Some(1 << j) } else { None })
                .sum();

            if rules.contains(&cur) {
                new_state.push_back(true);
            } else {
                new_state.push_back(false);
            }
        }

        cur_state = new_state;

        let new_sum = (start..)
            .zip(cur_state.iter())
            .filter_map(|(i, &v)| if v { Some(i) } else { None })
            .sum::<isize>();

        let new_diff = new_sum - old_sum;
        if new_diff == old_diff {
            println!("{} {}", k, new_diff);
        }
        old_sum = new_sum;
        old_diff = new_diff;
    }
    old_sum
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_a1() {
        let (state, rules) = parse("input/test0");
        let ans = evolve(state, rules, 20);
        assert_eq!(ans, 325);
    }

    #[test]
    fn test_a0() {
        let (state, rules) = parse("input/input");
        let ans = evolve(state, rules, 20);
        assert_eq!(ans, 1447);
    }
}

fn main() {
    let (state, rules) = parse("input/input");
    // let (state, rules) = parse("input/test0");

    let now = Instant::now();
    // let ans_b = evolve(state, rules, 50000000000);
    let ans_b = evolve(state.clone(), rules.clone(), 100);
    let elapsed = now.elapsed();
    println!("{} ({:?})", ans_b, elapsed);

    let num: usize = 50000000000;
    // let num: usize = 100000;
    // println!("{}",evolve(state.clone(), rules.clone(), num));
    // set the following numbers by observing the output from the previous prints
    let last_transient: usize = 89;
    let steady: isize = 21;
    println!(
        "{}",
        (num - last_transient) as isize * steady + evolve(state, rules, last_transient)
    );
}
