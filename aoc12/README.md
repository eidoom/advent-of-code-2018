# [12](https://gitlab.com/eidoom/advent-of-code-2018/-/tree/master/aoc12)
* [Question](https://adventofcode.com/2018/day/12)
## Concepts
* This problem is a one-dimensional [cellular automaton](https://computing-blog.netlify.app/post/cellular-automata/)
* I found that higher-level/more functional contructs like
    ```rust
    let cur = (0..5)
        .into_iter()
        .filter_map(|j| if cur_state[i + j] { Some(1 << j) } else { None })
        .sum();
    ```
    have [zero-overhead](https://doc.rust-lang.org/book/ch13-04-performance.html) compared to lower-level/more imperative code, forthis example
    ```rust
    let mut cur = 0;
    for j in 0..5 {
        if cur_state[i + j] {
            cur += 1 << j;
        }
    }
    ```
    at least by looking at run time.
* I used a [`VecDeque`](https://doc.rust-lang.org/std/collections/struct.VecDeque.html) for the state for fast prepends
* a [filter_map](https://doc.rust-lang.org/std/iter/trait.Iterator.html#method.filter_map) can be used on an [iterator](https://doc.rust-lang.org/std/iter/trait.Iterator.html) to concisely only map elements meeting a criterion
* Left bitshift in Rust with [`<<`](https://doc.rust-lang.org/std/ops/trait.Shl.html)
* You can iterate directly on a [`Range`](https://doc.rust-lang.org/std/ops/struct.Range.html), like `(0..5).into_iter()...`
* [`zip`](https://doc.rust-lang.org/std/iter/trait.Iterator.html#method.zip) can be used like [`enumerate`](https://doc.rust-lang.org/std/iter/trait.Iterator.html#method.enumerate), but starting with any number
* The code went through several iterations, specifically how to match cells with rules
    * I [started](https://gitlab.com/eidoom/advent-of-code-2018/-/blob/f839623af392241318255574b35af3c84f7a4e63/aoc12/src/main.rs) by recording the live rules (else death) as [`String`s](https://doc.rust-lang.org/std/string/struct.String.html) in a [`HashSet`](https://doc.rust-lang.org/std/collections/struct.HashSet.html)
    * [Then](https://gitlab.com/eidoom/advent-of-code-2018/-/blob/b771186c9bbcebf3a35995b29367763175fdcd0a/aoc12/src/main.rs), since cells are either alive `#` or dead `.`, I represented this with a `bool`, which theoretically saves on memory, but didn't make much difference to runtime
    * [Next](https://gitlab.com/eidoom/advent-of-code-2018/-/blob/a1b8d88213da833198164d188b0862aeadb79fdc/aoc12/src/main.rs), I optimised this by converting the rules to `int`s (`usize`) by parsing them as binary numbers and storing them in a [`Vec`](https://doc.rust-lang.org/std/vec/struct.Vec.html)
    * I [found](https://gitlab.com/eidoom/advent-of-code-2018/-/blob/1a85709573c5256c8ba29c488aa563e2534273e3/aoc12/src/main.r0s) that a [linear search](https://doc.rust-lang.org/std/vec/struct.Vec.html#method.contains) of the rules for a matching `int` was faster than the [hash search](https://doc.rust-lang.org/std/collections/struct.HashSet.html#method.contains). Also, using a [binary search](https://doc.rust-lang.org/std/vec/struct.Vec.html#method.binary_search) of a [sorted](https://doc.rust-lang.org/std/vec/struct.Vec.html#method.sort_unstable) rules `Vec` was slower than the linear search.
    * At this point, I ran out of ideas. The flamegraph shows the rule search is still the bottleneck, followed by converting each 5-cell to a number:
    ![flamegraph](flamegraph-linear-search.svg)
* With the evaluation time still impractically long for part two, I was reminded of [day 12 of 2019](https://adventofcode.com/2019/day/12) (no coincidence that its the same day), so started looking for a pattern in the number of plants in each generation.
    * It turned out that after 86 generations for `test0` (and 89 for `input`), the change in the result between subsequent generations (`delta`) became steady at 20 (21). So the answer can be found as:
    ```
    [last transient generation result] + [number of remaining generations] * [steady delta]
    ```
