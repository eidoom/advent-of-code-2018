extern crate regex;

extern crate common;

use std::collections::HashSet;
use std::time::Instant;

use regex::Regex;

#[derive(Debug)]
struct Claim {
    id: usize,
    coords: HashSet<[usize; 2]>,
}

impl Claim {
    fn from(id_: usize) -> Self {
        Self {
            id: id_,
            coords: HashSet::<[usize; 2]>::new(),
        }
    }

    fn gen_coords(&mut self, i: usize, j: usize, w: usize, h: usize) {
        for ii in i..(i + w) {
            for jj in j..(j + h) {
                self.coords.insert([ii, jj]);
            }
        }
    }
}

fn regex_parser(filename: &str) -> Vec<Claim> {
    let mut claims = Vec::new();
    let data = common::read_in(filename);
    let re = Regex::new(r"#([0-9]+) @ ([0-9]+),([0-9]+): ([0-9]+)x([0-9]+)")
        .expect("Regex pattern failed");
    for line in data {
        let mat = re.captures(&line).expect("Regex match failed");

        let mut buf: [usize; 5] = [0; 5];

        for i in 0..5 {
            let cap = mat.get(i + 1).expect("Capture failed");
            let val = (&line[cap.start()..cap.end()])
                .parse::<usize>()
                .expect("Couldn't parse string");
            buf[i] = val;
        }

        let mut claim = Claim::from(buf[0]);
        claim.gen_coords(buf[2], buf[1], buf[4], buf[3]);
        // println!("{:?}",claim);

        claims.push(claim);
    }
    claims
}

fn overlaps(claims: Vec<Claim>) -> (usize, HashSet<usize>) {
    let mut overlaps = HashSet::<&[usize; 2]>::new();
    let mut separate: HashSet<usize> = claims.iter().map(|c| c.id).collect();

    for i in 0..claims.len() {
        for j in (i + 1)..claims.len() {
            let intersection = (claims[i].coords).intersection(&claims[j].coords);
            if intersection.clone().collect::<Vec<&[usize; 2]>>().len() > 0 {
                separate.remove(&claims[i].id);
                separate.remove(&claims[j].id);

                for x in intersection {
                    overlaps.insert(x);
                }
            }
        }
    }
    (overlaps.len(), separate)
}

#[cfg(test)]
mod tests {
    use super::*;
    fn test(i: usize, r: usize) {
        let data = regex_parser(&("input/test".to_string() + &i.to_string()));
        let (ans, _) = overlaps(data);
        assert_eq!(r, ans);
    }
    #[test]
    fn test0() {
        test(0, 4)
    }
    #[test]
    fn test1() {
        test(1, 9)
    }
}

fn main() {
    println!("Reading in data...");
    let claims = regex_parser("input/input");
    // let claims = regex_parser("input/test0");
    println!("  Read in {} claims", claims.len());
    println!("Solving for overlaps...");
    let now = Instant::now();
    let (ansa, ansb) = overlaps(claims);
    let elapsed = now.elapsed();
    println!(
        "  Overlaps: {} ; separate: {:?} ({:?})",
        ansa, ansb, elapsed
    );
}
