# [3](https://gitlab.com/eidoom/advent-of-code-2018/-/tree/master/aoc03)
* [Question](https://adventofcode.com/2018/day/3)
## Concepts
* I relied on `HashSet`s to do this one. It's quite slow.
    ![flamegraph](flamegraph.svg)
