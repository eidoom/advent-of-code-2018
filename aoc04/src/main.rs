extern crate num;
extern crate regex;

extern crate common;

use std::cmp::Ordering;
use std::collections::HashMap;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
enum State {
    Begin,
    Sleep,
    Wake,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
struct Entry {
    month: u8,
    day: u8,
    hour: u8,
    minute: u8,
    state: State,
    id: u16,
}

impl PartialOrd for Entry {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Entry {
    fn cmp(&self, other: &Self) -> Ordering {
        let cmp_month = self.month.cmp(&other.month);
        let cmp_day = self.day.cmp(&other.day);
        let cmp_hour = self.hour.cmp(&other.hour);
        let cmp_minute = self.minute.cmp(&other.minute);
        cmp_month.then(cmp_day).then(cmp_hour).then(cmp_minute)
    }
}

fn max<T, U>(map: &HashMap<T, U>) -> (T, U)
where
    T: num::Zero + Copy,
    U: num::Zero + PartialOrd + Copy,
{
    let mut max_key = num::zero();
    let mut max_value = num::zero();
    for (&key, &value) in map.iter() {
        if value > max_value {
            max_value = value;
            max_key = key;
        }
    }
    (max_key, max_value)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_a() {
        let data = parse("input/test0");
        assert_eq!(find_a(&data), 240)
    }

    #[test]
    fn test_b() {
        let data = parse("input/test0");
        assert_eq!(find_b(&data), 4455)
    }
}

fn parse(filename: &str) -> Vec<Entry> {
    let data = common::read_in(filename);

    let mut entries = Vec::<Entry>::new();

    let re = regex::Regex::new(r"\d{2,4}").expect("Regex pattern failed");

    let mut nid: u16;

    for line in data {
        let st = match line.chars().nth(19).expect("Couldn't get character") {
            'G' => Some(State::Begin),
            'f' => Some(State::Sleep),
            'w' => Some(State::Wake),
            _ => None,
        }
        .expect("Couldn't parse state");

        if st == State::Begin {
            let subs = &line[26..30];
            let mat = re.find(&subs).expect("Regex match failed");
            nid = (&subs[mat.start()..mat.end()])
                .parse::<u16>()
                .expect("Couldn't parse string");
        } else {
            nid = 0;
        }

        let entry = Entry {
            month: (&line[6..8]).parse::<u8>().expect("Couldn't parse year"),
            day: (&line[9..11]).parse::<u8>().expect("Couldn't parse day"),
            hour: (&line[12..14]).parse::<u8>().expect("Couldn't parse hour"),
            minute: (&line[15..17])
                .parse::<u8>()
                .expect("Couldn't parse minute"),
            state: st,
            id: nid,
        };

        entries.push(entry);
    }

    entries.sort_unstable();

    for i in 0..entries.len() {
        if entries[i].id == 0 {
            entries[i].id = entries[i - 1].id;
        }
    }

    entries
}

fn find_a(entries: &[Entry]) -> u32 {
    let mut guards = HashMap::<u16, u16>::new();

    for (i, entry) in entries.iter().enumerate() {
        if entry.state == State::Wake {
            let dur = entry.minute - entries[i - 1].minute;
            *guards.entry(entry.id).or_insert(0) += dur as u16;
        }
    }

    let dozer = max(&guards).0;
    // println!("ID: {}", dozer);

    let mut minutes = HashMap::<u8, u8>::new();

    for (i, entry) in entries.iter().enumerate() {
        if entry.id == dozer && entry.state == State::Wake {
            for i in entries[i - 1].minute..entry.minute {
                *minutes.entry(i).or_insert(0) += 1;
            }
        }
    }

    let fav_min = max(&minutes).0;
    // println!("Minute: {}", fav_min);

    let ans: u32 = dozer as u32 * fav_min as u32;
    // println!("Answer: {}", ans);
    ans
}

fn find_b(entries: &[Entry]) -> u32 {
    let mut everyone = HashMap::<u16, HashMap::<u8, u8>>::new();

    for (i, entry) in entries.iter().enumerate() {
        if entry.state == State::Wake {
            for i in entries[i - 1].minute..entry.minute {
                *everyone.entry(entry.id).or_insert(HashMap::<u8, u8>::new()).entry(i).or_insert(0) += 1;
            }
        }
    }

    // println!("{:?}\n", everyone);

    let mut guard_mins = HashMap::<u16, u8>::new();
    let mut guard_durs = HashMap::<u16, u8>::new();

    for (&id, mins) in everyone.iter() {
        let (min, dur) = max(mins);
        guard_mins.insert(id, min);
        guard_durs.insert(id, dur);
    }

    // println!("{:?}\n", guard_mins);
    // println!("{:?}\n", guard_durs);

    let guard = max(&guard_durs).0;
    // println!("Guard ID: {}", guard);

    let minute = guard_mins[&guard];
    // println!("Favourite minute: {}", minute);

    guard as u32 * minute as u32
}

fn main() {
    let data = parse("input/input");
    println!("A: {}", find_a(&data));
    println!("B: {}", find_b(&data));
}
