extern crate common;

fn run(filename: &str) {
    let data = common::read_in(filename);
    println!("Hello, world!\n{:?}", data);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_a() {
        run("input/test0");
        assert_eq!(1, 1);
    }
}

fn main() {
    run("input/input");
}
