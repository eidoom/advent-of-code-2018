# [18](https://gitlab.com/eidoom/advent-of-code-2018/-/tree/master/aoc18)
* [Question](https://adventofcode.com/2018/day/18)
## Concepts
* 3 state cellular automaton with Moore neighbourhood
* Use a slice `&[T]` rather than a `&Vec<T>`
    * See https://stackoverflow.com/a/40439278, https://stackoverflow.com/q/40006219
