extern crate common;

#[derive(Clone, PartialEq)]
enum State {
    Open,
    Tree,
    Yard,
}

use State::{Open, Tree, Yard};

impl std::fmt::Display for State {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match *self {
            Open => write!(f, "."),
            Tree => write!(f, "|"),
            Yard => write!(f, "#"),
        }
    }
}

impl State {
    fn from(input: char) -> Result<Self, String> {
        match input {
            '.' => Ok(Open),
            '|' => Ok(Tree),
            '#' => Ok(Yard),
            other => Err(format!(
                    "Error translating input to internal data format: unknown character `{}`; I only recognise `.`, `|`, `#`",
                    other)),
        }
    }
}

#[derive(Clone)]
struct Grid {
    // a Vec<State> with custom indexing might be faster
    data: Vec<Vec<State>>,
    side: usize,
}

impl Grid {
    fn from(input: &[String]) -> Self {
        let n = input.len();

        let mut output = vec![vec![Open; n + 2]; n + 2];

        for i in 0..n {
            for j in 0..n {
                output[i + 1][j + 1] = State::from(input[i].chars().nth(j).unwrap()).unwrap();
            }
        }

        // let grid: Vec<Vec<State>> = input
        //     .iter()
        //     .map(|l| l.chars().map(|c| State::from(c).unwrap()).collect())
        //     .collect();

        Self {
            data: output,
            side: n,
        }
    }

    fn global(&self, state: State) -> usize {
        self.data[1..=self.side]
            .iter()
            .map(|l| l[1..=self.side].iter().filter(|&x| *x == state).count())
            .sum()
    }

    fn resource_value(&self) -> usize {
        self.global(Tree) * self.global(Yard)
    }

    // how to make lazy? ie if testing result < 3, stop once hit 3
    fn local_count(&self, i: usize, j: usize, state: State) -> usize {
        let mut c = 0;

        c += self[i - 1][j - 1..=j + 1]
            .iter()
            .filter(|&n| *n == state)
            .count();

        if self[i][j - 1] == state {
            c += 1
        };

        if self[i][j + 1] == state {
            c += 1
        };

        c += self[i + 1][j - 1..=j + 1]
            .iter()
            .filter(|&n| *n == state)
            .count();

        c
    }

    // .|.
    // |.|  >  |
    // ...

    // .#.
    // #|#  >  #
    // ...

    // ...
    // .#.  >  .
    // ...

    fn step(&mut self) {
        let prev = self.clone();

        for i in 1..=self.side {
            for j in 1..=self.side {
                // println!("{}", prev[i][j]);
                match prev[i][j] {
                    Open => {
                        if prev.local_count(i, j, Tree) >= 3 {
                            self[i][j] = Tree;
                        }
                    }
                    Tree => {
                        if prev.local_count(i, j, Yard) >= 3 {
                            self[i][j] = Yard;
                        }
                    }
                    Yard => {
                        if prev.local_count(i, j, Tree) == 0 || prev.local_count(i, j, Yard) == 0 {
                            self[i][j] = Open;
                        }
                    }
                }
            }
        }
    }
}

impl std::fmt::Display for Grid {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let mut out = String::new();
        for i in 1..=self.side {
            for j in 1..=self.side {
                out.push(match self[i][j] {
                    Open => '.',
                    Tree => '|',
                    Yard => '#',
                });
            }
            out.push('\n');
        }
        write!(f, "{}", out)
    }
}

impl std::ops::Index<usize> for Grid {
    type Output = Vec<State>;

    fn index(&self, i: usize) -> &Self::Output {
        &self.data[i]
    }
}

impl std::ops::IndexMut<usize> for Grid {
    fn index_mut(&mut self, i: usize) -> &mut Self::Output {
        &mut self.data[i]
    }
}

fn run(mut grid: Grid, mins: usize) -> usize {
    for _ in 0..mins {
        grid.step();
        // println!("{}", grid);
    }

    // println!("{}", grid);
    grid.resource_value()
}

fn find(mut grid: Grid, mins: usize) -> usize {
    let mut res = Vec::<usize>::new();

    let mut cycle_start = 0;
    let mut cycle_len = 0;

    // identify cycle
    for i in 0..mins {
        grid.step();
        let n = grid.resource_value();
        if let Ok(p) = res.binary_search(&n) {
            if &res[p - 1] == res.last().unwrap() {
                if res[p - 2] == res[res.len() - 2] {
                    cycle_start = p - 2;
                    cycle_len = i - p;
                    println!(
                        "{} {} {} {}",
                        i,
                        res[p - 2],
                        cycle_len,
                        cycle_start
                    );
                    break;
                }
            }
        }
        res.push(n);
    }

    for _ in 0..cycle_len-2 {
        grid.step();
    }
    println!("{}", grid.resource_value());

    let phase = (mins - cycle_start) % cycle_len;

    println!("{}", phase);

    for _ in 0..phase-1 {
        grid.step()
    }

    // println!("{}", phase + cycle_start);

    grid.resource_value()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_a() {
        let data = common::read_in("input/test0");
        let grid = Grid::from(&data);
        assert_eq!(run(grid, 10), 1147);
    }
}

fn main() {
    let data = common::read_in("input/input");
    let grid = Grid::from(&data);
    // println!("{}", grid);

    println!("a: {}", run(grid.clone(), 10));

    println!("b: {}", find(grid.clone(), 1000000000));
    // println!("b: {}", run(grid.clone(), 615));
}
