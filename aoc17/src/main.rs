extern crate common;

#[derive(Debug, Clone, Copy)]
struct Coord {
    x: usize,
    y: usize,
}

impl Coord {
    fn new() -> Self {
        Self { x: 0, y: 0 }
    }

    fn from(x_: usize, y_: usize) -> Self {
        Self { x: x_, y: y_ }
    }

    fn below(mut self) -> Self {
        self.y += 1;
        self
    }

    fn above(mut self) -> Self {
        self.y -= 1;
        self
    }

    fn left(mut self) -> Self {
        self.x -= 1;
        self
    }

    fn right(mut self) -> Self {
        self.x += 1;
        self
    }
}

impl std::fmt::Display for Coord {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

fn find_clay(filename: &str) -> Vec<Coord> {
    let data = common::read_in(filename);
    let mut clay: Vec<Coord> = Vec::new();
    for mut pair in data
        .iter()
        .map(|line| line.splitn(2, ", ").map(|q| q.splitn(2, "=")))
    {
        let mut first = pair.next().unwrap();
        let mut second = pair.next().unwrap();
        if first.next() == Some("x") {
            let x = first.next().unwrap().parse::<usize>().unwrap();
            let lims: Vec<usize> = second
                .nth(1)
                .unwrap()
                .splitn(2, "..")
                .map(|e| e.parse::<usize>().unwrap())
                .collect();
            for yy in lims[0]..lims[1] + 1 {
                clay.push(Coord::from(x, yy));
            }
        } else {
            let y = first.next().unwrap().parse::<usize>().unwrap();
            let lims: Vec<usize> = second
                .nth(1)
                .unwrap()
                .splitn(2, "..")
                .map(|e| e.parse::<usize>().unwrap())
                .collect();
            for xx in lims[0]..lims[1] + 1 {
                clay.push(Coord::from(xx, y));
            }
        }
    }
    clay
}

struct Material;

impl Material {
    const CLAY: char = '#';
    const SAND: char = '.';
    const WATER: char = '~';
    const SPRING: char = '+';
}

#[derive(PartialEq, Eq)]
enum Direction {
    Up,
    Right,
    Down,
    Left,
}

struct Ground {
    width: usize,
    height: usize,
    state: Vec<char>,
    spring: Coord,
}

impl Ground {
    fn new(xlen: usize, ylen: usize) -> Self {
        Self {
            width: xlen,
            height: ylen,
            state: vec![Material::SAND; xlen * ylen],
            spring: Coord::new(),
        }
    }

    fn setxy(&mut self, x: usize, y: usize, v: char) {
        self.state[y * self.width + x] = v;
    }

    fn setp(&mut self, p: &Coord, v: char) {
        self.setxy(p.x, p.y, v);
    }

    fn getxy(&self, x: usize, y: usize) -> Option<&char> {
        self.state.get(y * self.width + x)
    }

    fn getp(&self, p: &Coord) -> Option<&char> {
        self.getxy(p.x, p.y)
    }

    fn from(clay: &Vec<Coord>) -> Self {
        let gspring = Coord::from(500, 0);
        let xs = clay.iter().map(|p| p.x);
        let xmin = xs.clone().min().unwrap();
        let xmax = xs.max().unwrap();
        let xlen = xmax - xmin + 1;
        let ys = clay.iter().map(|p| p.y);
        let ymin = std::cmp::min(gspring.y, ys.clone().min().unwrap());
        let ymax = ys.max().unwrap();
        let ylen = ymax - ymin + 1;
        let mut new = Self::new(xlen, ylen);
        for p in clay {
            new.setxy(p.x - xmin, p.y, Material::CLAY);
        }
        new.spring.x = gspring.x - xmin;
        new.spring.y = gspring.y;
        new.setxy(new.spring.x, new.spring.y, Material::SPRING);
        new
    }

    fn evolve(&mut self) {
        // let stdin = std::io::stdin();
        // let input = &mut String::new();
        // println!("{}", self);
        let mut stack = vec![(Direction::Down, self.spring.below())];
        let mut path = vec![self.spring];
        while path.len() != 0 {
            while stack.len() != 0 {
                let (dir, cur) = stack.pop().unwrap();
                if self.getp(&cur) == Some(&Material::SAND) {
                    self.setp(&cur, Material::WATER);
                    path.push(cur);
                    stack.push((Direction::Down, cur.below()));
                } else if self.getp(&cur) == Some(&Material::WATER) {
                    if dir == Direction::Up {
                        if cur.x > 0 {
                            stack.push((Direction::Left, cur.left()));
                        }
                        stack.push((Direction::Right, cur.right()));
                    } else if dir == Direction::Down {
                        stack.push((Direction::Up, cur.above()));
                    }
                } else if self.getp(&cur) == Some(&Material::CLAY) {
                    if dir == Direction::Down {
                        stack.push((Direction::Up, cur.above()));
                    }
                }
                // println!("{}", self);
                // stdin.read_line(input).unwrap();
            }
            let mut back1 = path.pop().unwrap();
            if back1.y == self.height - 1 {
                break;
            }
            let mut back2 = path.pop().unwrap();
            while back1.y == back2.y {
                back1 = back2;
                back2 = path.pop().unwrap();
            }
            if back2.x > 0 {
                stack.push((Direction::Left, back2.left()));
            }
            stack.push((Direction::Right, back2.right()));
        }
        println!("{}", self);
    }

    fn count(&self) -> usize {
        self.state.iter().fold(
            0,
            |acc, &x| if x == Material::WATER { acc + 1 } else { acc },
        )
    }
}

impl std::fmt::Display for Ground {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let mut out: String = self.state.iter().collect();
        for i in (self.width..(self.width + 1) * self.height).step_by(self.width + 1) {
            out.insert(i, '\n');
        }
        write!(f, "{}", out)
    }
}

fn run(clay: &Vec<Coord>) -> usize {
    let mut ground = Ground::from(clay);
    ground.evolve();
    ground.count()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_a() {
        assert_eq!(run(&find_clay("input/test0")), 57);
    }
}

fn main() {
    let clay = find_clay("input/input");
    // let clay = find_clay("input/test0");
    let a = run(&clay);
    println!("{}", a);
}
