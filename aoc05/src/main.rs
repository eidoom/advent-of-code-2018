use std::collections::HashSet;

extern crate common;

fn collapse(polymer: &mut String) -> usize {
    let mut i = 0;

    while i < polymer.len() - 1 {
        let this = polymer.chars().nth(i).expect("Couldn't get character");
        let next = polymer.chars().nth(i + 1).expect("Couldn't get character");
        if (this.is_lowercase() && next == this.to_ascii_uppercase())
            || (this.is_uppercase() && next == this.to_ascii_lowercase())
        {
            polymer.remove(i);
            polymer.remove(i);
            if i > 0 {
                i -= 1;
            }
        } else {
            i += 1;
        }
    }

    // println!("{:?}", polymer);

    polymer.len()
}

fn optimise(polymer: String) -> usize {
    let mut units = HashSet::<char>::new();

    for unit in polymer.chars() {
        units.insert(unit.to_ascii_uppercase());
    }

    let mut shortest = 1000000;
    for unit in units {
        let mut poly = polymer.replace(&[unit, unit.to_ascii_lowercase()][..], "");
        let len = collapse(&mut poly);
        if len < shortest {
            shortest = len;
        }
        // println!("{} {:?} {}", unit, poly, len);
    }

    shortest
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_a() {
        let mut polymer = common::read_in_single("input/test0");
        assert_eq!(collapse(&mut polymer), 10)
    }

    #[test]
    fn test_b() {
        let polymer = common::read_in_single("input/test0");
        assert_eq!(optimise(polymer), 4)
    }
}

fn main() {
    let polymer = common::read_in_single("input/input");

    let ans_a = collapse(&mut polymer.clone());
    println!("{}", ans_a);

    let ans_b = optimise(polymer);
    println!("{}", ans_b);
}
