# [5](https://gitlab.com/eidoom/advent-of-code-2018/-/tree/master/aoc05)
* [Question](https://adventofcode.com/2018/day/5)
## Concepts
* [sets](https://doc.rust-lang.org/std/collections/struct.HashSet.html)
* [Strings](https://doc.rust-lang.org/std/string/struct.String.html#method.as_mut_ptr), [chars](https://doc.rust-lang.org/std/primitive.char.html#method.to_ascii_uppercase), [removing chars from strings](https://users.rust-lang.org/t/fast-removing-chars-from-string/24554)
