extern crate common;

use std::{collections::HashSet, env};

fn read_in_int(filename: &str) -> Vec<i32> {
    let data = common::read_in(filename);

    data.iter()
        .map(|l| {
            l.parse::<i32>()
                .expect(&("Couldn't parse string: ".to_string() + &l))
        })
        .collect()
}

fn first_twice(data: Vec<i32>) -> i32 {
    let mut curs = HashSet::<i32>::new();
    let mut cur = 0;
    let mut i = 0;
    loop {
        curs.insert(cur);
        cur += data[i];
        if curs.contains(&cur) {
            return cur;
        }
        i = (i + 1) % data.len();
    }
}

#[cfg(test)]
mod tests {
    fn test(i: u8, r: i32) {
        let data = super::read_in_int(&("input/test".to_string() + &i.to_string()));
        let ans = super::first_twice(data);
        assert_eq!(r, ans);
    }
    #[test]
    fn test0() {
        test(0, 2)
    }
    #[test]
    fn test1() {
        test(1, 0)
    }
    #[test]
    fn test2() {
        test(2, 10)
    }
    #[test]
    fn test3() {
        test(3, 5)
    }
    #[test]
    fn test4() {
        test(4, 14)
    }
}

fn main() {
    let filename = env::args().nth(1).unwrap_or("input/input".to_string());
    let data = read_in_int(&filename);

    let total: i32 = data.iter().sum();
    println!("Sum: {}", total);

    let ans = first_twice(data);
    println!("First twice: {}", ans);
}
