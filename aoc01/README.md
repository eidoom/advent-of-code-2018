# [1](https://gitlab.com/eidoom/advent-of-code-2018/-/tree/master/aoc01)
* [Question](https://adventofcode.com/2018/day/1)
## Concepts
* Passing command line arguments to a Rust script with [`env`](https://doc.rust-lang.org/std/env/)
* Sets with [`HashSet`](https://doc.rust-lang.org/std/collections/struct.HashSet.html)
* unpacking [`Option`](https://doc.rust-lang.org/std/option/index.html)s
* [`parse`](https://doc.rust-lang.org/std/string/struct.String.html#method.parse) string as `int`
