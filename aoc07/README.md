# [7](https://gitlab.com/eidoom/advent-of-code-2018/-/tree/master/aoc07)
* [Question](https://adventofcode.com/2018/day/7)
## Concepts
* [Topological sorting](https://en.wikipedia.org/wiki/Topological_sorting) of a [DAG](https://en.wikipedia.org/wiki/Directed_acyclic_graph)
    * [Crate petgraph](https://docs.rs/petgraph/0.5.1/petgraph/)
        * how to customise order of degenerate branches with `toposort`, since currently wrong for question?
        * I decided to write my own implementation instead for pedagogy (left petgraph partial solution in comments)
    * Really just some sorting and a depth first traversal, for test at least
* [VecDeque](https://doc.rust-lang.org/std/collections/struct.VecDeque.html)
