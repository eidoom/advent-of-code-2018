// extern crate petgraph;

extern crate common;

use std::collections::HashMap;
// use std::collections::BTreeMap;
use std::collections::VecDeque;

// use petgraph::{
//     algo::toposort,
//     graph::{node_index, DiGraph},
//     prelude::NodeIndex,
// };

fn nth_char(line: &str, n: usize) -> char {
    line.chars().nth(n).unwrap()
}

// fn num(c: char) -> NodeIndex {
//     node_index(c as usize - 65)
// }

// // using petgraph (fail)

// type Edge = [char; 2];

// fn run_pg(filename: &str) -> String {
//     let raw = common::read_in(filename);

//     let edges: Vec<Edge> = raw
//         .iter()
//         .map(|l| [nth_char(l, 5), nth_char(l, 36)])
//         .collect();
//     // println!("{:?}", edges);

//     let mut unique = edges.iter().flatten().collect::<Vec<&char>>();
//     unique.sort_unstable();
//     unique.dedup();
//     // println!("{:?}", unique);

//     let indices = edges
//         .iter()
//         .map(|&[a, b]| (num(a), num(b)))
//         .collect::<Vec<(NodeIndex, NodeIndex)>>();
//     // println!("{:?}", indices);

//     let mut graph = DiGraph::<char, ()>::new();
//     for &letter in unique {
//         graph.add_node(letter);
//     }
//     graph.extend_with_edges(&indices);
//     // println!("{:?}", graph);

//     let sorted: String = toposort(&graph, None)
//         .unwrap()
//         .iter()
//         .map(|&i| graph.node_weight(i).unwrap())
//         .collect();
//     // println!("{:?}", sorted);
//     sorted
// }

type Nodes = HashMap<char, Vec<char>>;

fn run(filename: &str) -> String {
    let raw = common::read_in(filename);

    let mut nodes = Nodes::new();

    for line in raw {
        let parent = nth_char(&line, 5);
        let child = nth_char(&line, 36);
        // println!("{} {}", parent, child);

        if !nodes.contains_key(&child) {
            nodes.insert(child, Vec::new());
        }

        nodes
            .entry(parent)
            .and_modify(|node| node.push(child))
            .or_insert(vec![child]);
    }

    for children in nodes.values_mut() {
        children.sort_unstable_by(|a, b| b.cmp(a));
    }

    fn visit(nodes_: &Nodes, node: char, sorted_: &mut VecDeque<char>) {
        // println!("visiting {}", node);
        if !sorted_.contains(&node) {
            match nodes_.get(&node) {
                Some(children) => {
                    for &child in children {
                        visit(&nodes_, child, sorted_);
                    }
                }
                None => (),
            }
            sorted_.push_front(node);
        }
    }

    let mut sorted = VecDeque::<char>::new();

    let mut names: Vec<char> = nodes.keys().map(|&k| k).collect();
    names.sort_unstable_by(|a, b| b.cmp(a));
    // println!("{:?}", names);

    for &node in &names {
        visit(&nodes, node, &mut sorted);
    }

    sorted.iter().collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test0() {
        assert_eq!("CABDFE", run("input/test0"));
    }

    #[test]
    fn test1() {
        assert_eq!("AEJBC", run("input/test1"));
    }

    #[test]
    fn test2() {
        assert_eq!("ABDC", run("input/test2"));
    }

    #[test]
    fn test3() {
        assert_eq!("ABNW", run("input/test3"));
    }

    #[test]
    fn test4() {
        assert_eq!("AFGYZ", run("input/test4"));
    }
}

fn main() {
    // let ans = run("input/input");
    let ans = run("input/test0");
    println!("{:?}", ans);
}
