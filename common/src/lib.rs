use std::{
    fs::{File,read_to_string},
    io::{BufRead, BufReader},
    vec::Vec,
};

pub fn read_in_whole(filename: &str) -> String {
    read_to_string(filename).expect(&("Can't open file ".to_string() + filename)).trim().to_string()
}

pub fn read_in_single(filename: &str) -> String {
    let file = File::open(filename).expect(&("Can't open file ".to_string() + filename));
    let mut buf = BufReader::new(file);
    let mut line = String::new();
    buf.read_line(&mut line)
        .expect(&("Can't read file ".to_string() + filename));
    line.pop();
    line
}

pub fn read_in(filename: &str) -> Vec<String> {
    let file = File::open(filename).expect(&("Can't open file ".to_string() + filename));
    let buf = BufReader::new(file);

    buf.lines()
        .map(|l| l.expect(&("Could not parse line in ".to_string() + filename)))
        .filter(|l| l != "")
        .collect()
}
