# [common](https://gitlab.com/eidoom/advent-of-code-2018/-/tree/master/common)

Library of code used in multiple days.

## Concepts
* Reading data from files with [BufReader](https://doc.rust-lang.org/std/io/struct.BufReader.html)
