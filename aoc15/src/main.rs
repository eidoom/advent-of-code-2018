extern crate common;

#[derive(Debug, PartialEq, Eq)]
struct Pos {
    i: usize,
    j: usize,
}

impl std::fmt::Display for Pos {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "({}, {})", self.i, self.j)
    }
}

impl PartialOrd for Pos {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Pos {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        let cmp_i = self.i.cmp(&other.i);
        let cmp_j = self.j.cmp(&other.j);
        cmp_i.then(cmp_j)
    }
}

impl Pos {
    fn from(ii: usize, jj: usize) -> Self {
        Self {
            i: ii,
            j: jj,
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
struct Unit {
    q: Pos,
    team: char,
    ap: usize, // attack power
    hp: isize, // hit points
}

impl Unit {
    fn from(i: usize, j: usize, t: char) -> Self {
        Self {
            q: Pos::from(i, j),
            team: t,
            ap: 3,
            hp: 200,
        }
    }
}

impl PartialOrd for Unit {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Unit {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.q.cmp(&other.q)
    }
}

#[derive(Debug)]
struct Node {
    x: Pos,
    d: usize,
}

impl Node {
    fn from(i: usize, j:usize) -> Self {
        Self {
            x: Pos::from(i,j),
            d: usize::MAX,
        }
    }
}

struct Cave {
    back: Vec<Vec<char>>,
    w: usize,
    h: usize,
    units: Vec<Unit>,
}

impl Cave {
    fn from(d: Vec<String>) -> Self {
        let mut dd: Vec<Vec<char>> = d.iter().map(|r| r.chars().collect()).collect();
        let ww = dd[0].len();
        let hh = dd.len();
        let mut us = Vec::<Unit>::new();
        for i in 0..hh {
            for j in 0..ww {
                if dd[i][j] == 'G' {
                    us.push(Unit::from(i, j, 'G'));
                    dd[i][j] = '.';
                } else if dd[i][j] == 'E' {
                    us.push(Unit::from(i, j, 'E'));
                    dd[i][j] = '.';
                }
            }
        }
        us.sort_unstable();
        Self {
            back: dd,
            w: ww,
            h: hh,
            units: us,
        }
    }

    fn populate(&self) -> Vec<Vec<char>> {
        let mut map = self.back.clone();
        for u in &self.units {
            map[u.q.i][u.q.j] = u.team;
        }
        map
    }

    fn open(&self, u: &Pos) -> Vec<Pos> {
        let map = self.populate();
        let mut os = Vec::new();
        let i = u.i;
        let j = u.j;
        if map[i + 1][j] == '.' {
            os.push(Pos::from(i + 1, j));
        }
        if map[i][j + 1] == '.' {
            os.push(Pos::from(i, j + 1));
        }
        if map[i][j - 1] == '.' {
            os.push(Pos::from(i, j - 1));
        }
        if map[i - 1][j] == '.' {
            os.push(Pos::from(i - 1, j));
        }
        os
    }
        
    // fn subpath(map: &Vec<Vec<char>>, s: &Pos, mut p: Vec<Node>) -> Vec<Node> {
    //     let i = s.i;
    //     let j = s.j;
    //     if map[i - 1][j] == '.' {
    //         p.push(Node::from(i - 1, j));
    //         p = Self::subpath(&map,s, p);
    //     }
    //     if map[i][j - 1] == '.' {
    //         p.push(Node::from(i, j - 1));
    //         p = Self::subpath(&map,s, p);
    //     }
    //     if map[i][j + 1] == '.' {
    //         p.push(Node::from(i, j + 1));
    //         p = Self::subpath(&map,s, p);
    //     }
    //     if map[i + 1][j] == '.' {
    //         p.push(Node::from(i + 1, j));
    //         p = Self::subpath(&map,s, p);
    //     }
    //     p
    // }

    fn path(&self, s: &Pos, f: &Pos) -> Vec<Node> {
        let map = self.populate();
        println!("{}", s);
        println!("{}", f);
        // {
            // let i = s.i;
            // let j = s.j;
            // let p = Vec::<Node>::new();
            // let q = Self::subpath(&map, s, p);
            // println!("{:?}", p);
        // }
        // let mut unvisited = Vec::new();
        // for i in 0..self.h {
        //     for j in 0..self.w {
        //         if map[i][j] == '.' {
        //             unvisited.push(Node::from(i, j));
        //         }
        //     }
        // }
        // println!("{:?}", unvisited);
        vec![]
    }
}

impl std::fmt::Display for Cave {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let mut one = self.populate();
        one.iter_mut().for_each(|r| r.push('\n'));
        let two: String = one.iter().flatten().collect();
        write!(f, "{}", two)
    }
}

fn run(filename: &str) {
    let raw = common::read_in(filename);
    let cave = Cave::from(raw);
    println!("{}", cave);
    for u in &cave.units[0..1] {
        // println!("{:?}", u);
        let mut enemies = Vec::<&Unit>::new();
        for v in &cave.units {
            if u.team != v.team {
                enemies.push(&v);
            }
        }
        // println!("{:?}", enemies);
        // let mut os = Vec::new();
        // for e in enemies {
        //     os.append(&mut cave.open(&e.q));
        // }
        // println!("{} -> {:?}", u.q, os);
        let _p = cave.path(&u.q, &enemies[0].q);
        // println!("{:?}", p);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_a() {
        run("input/test0");
        assert_eq!(1, 1);
    }
}

fn main() {
    // run("input/input");
    run("input/test2");
}
